<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<refentry id="smbtree.1">

<refmeta>
	<refentrytitle>smbtree</refentrytitle>
	<manvolnum>1</manvolnum>
	<refmiscinfo class="source">Samba</refmiscinfo>
	<refmiscinfo class="manual">User Commands</refmiscinfo>
	<refmiscinfo class="version">4.10.11</refmiscinfo>
</refmeta>


<refnamediv>
	<refname>smbtree</refname>
	<refpurpose>A text based smb network browser
	</refpurpose>
</refnamediv>

<refsynopsisdiv>
	<cmdsynopsis sepchar=" ">
		<literal>smbtree</literal>
		<arg choice="opt" rep="norepeat">-b</arg>
		<arg choice="opt" rep="norepeat">-D</arg>
		<arg choice="opt" rep="norepeat">-S</arg>
	</cmdsynopsis>
</refsynopsisdiv>

<refsect1>
	<title>DESCRIPTION</title>

	<para>This tool is part of the <citerefentry><refentrytitle>samba</refentrytitle>
	<manvolnum>7</manvolnum></citerefentry> suite.</para>

	<para><literal>smbtree</literal> is a smb browser program 
	in text mode. It is similar to the "Network Neighborhood" found 
	on Windows computers. It prints a tree with all 
	the known domains, the servers in those domains and 
	the shares on the servers.
	</para>
</refsect1>


<refsect1>
	<title>OPTIONS</title>

	<variablelist>
		<varlistentry>
		<term>-b|--broadcast</term>
		<listitem><para>Query network nodes by sending requests 
		as broadcasts instead of querying the local master browser.
		</para></listitem>
		</varlistentry>

		<varlistentry>
		<term>-D|--domains</term>
		<listitem><para>Only print a list of all 
		the domains known on broadcast or by the 
		master browser</para></listitem>
		</varlistentry>

		<varlistentry>
		<term>-S|--servers</term>
		<listitem><para>Only print a list of 
		all the domains and servers responding on broadcast or 
		known by the master browser. 
		</para></listitem>
		</varlistentry>

		
<varlistentry>
<term>-d|--debuglevel=level</term>
<listitem>
<para><replaceable>level</replaceable> is an integer 
from 0 to 10. The default value if this parameter is 
not specified is 0.</para>

<para>The higher this value, the more detail will be 
logged to the log files about the activities of the 
server. At level 0, only critical errors and serious 
warnings will be logged. Level 1 is a reasonable level for
day-to-day running - it generates a small amount of 
information about operations carried out.</para>

<para>Levels above 1 will generate considerable 
amounts of log data, and should only be used when 
investigating a problem. Levels above 3 are designed for 
use only by developers and generate HUGE amounts of log
data, most of which is extremely cryptic.</para>

<para>Note that specifying this parameter here will 
override the <link xmlns:xlink="http://www.w3.org/1999/xlink" linkend="LOGLEVEL" xlink:href="smb.conf.5.html#LOGLEVEL">log level</link> parameter
in the <filename moreinfo="none">smb.conf</filename> file.</para>
</listitem>
</varlistentry>
		

<varlistentry>
<term>-V|--version</term>
<listitem><para>Prints the program version number.
</para></listitem>
</varlistentry>

<varlistentry>
<term>-s|--configfile=&lt;configuration file&gt;</term>
<listitem><para>The file specified contains the 
configuration details required by the server.  The 
information in this file includes server-specific
information such as what printcap file to use, as well 
as descriptions of all the services that the server is 
to provide. See <filename moreinfo="none">smb.conf</filename> for more information.
The default configuration file name is determined at 
compile time.</para></listitem>
</varlistentry>

<varlistentry>
<term>-l|--log-basename=logdirectory</term>
<listitem><para>Base directory name for log/debug files. The extension
<constant>".progname"</constant> will be appended (e.g. log.smbclient, 
log.smbd, etc...). The log file is never removed by the client.
</para></listitem>
</varlistentry>

<varlistentry>
<term>--option=&lt;name&gt;=&lt;value&gt;</term>
<listitem><para>Set the
<citerefentry><refentrytitle>smb.conf</refentrytitle>
<manvolnum>5</manvolnum></citerefentry>
option "&lt;name&gt;" to value "&lt;value&gt;" from the command line.
This overrides compiled-in defaults and options read from the configuration
file.
</para></listitem>
</varlistentry>

		

<varlistentry>
<term>-N|--no-pass</term>
<listitem><para>If specified, this parameter suppresses the normal
password prompt from the client to the user. This is useful when
accessing a service that does not require a password. </para>

<para>Unless a password is specified on the command line or
this parameter is specified, the client will request a
password.</para>

<para>If a password is specified on the command line and this
option is also defined the password on the command line will
be silently ingnored and no password will be used.</para></listitem>
</varlistentry>

<varlistentry>
<term>-k|--kerberos</term>
<listitem><para>
Try to authenticate with kerberos. Only useful in
an Active Directory environment.
</para></listitem>
</varlistentry>


<varlistentry>
<term>-C|--use-ccache</term>
<listitem><para>
Try to use the credentials cached by winbind.
</para></listitem>
</varlistentry>


<varlistentry>
<term>-A|--authentication-file=filename</term>
<listitem><para>This option allows
you to specify a file from which to read the username and
password used in the connection.  The format of the file is
</para>

<para><programlisting format="linespecific">
username = &lt;value&gt;
password = &lt;value&gt;
domain   = &lt;value&gt;
</programlisting></para>

<para>Make certain that the permissions on the file restrict 
access from unwanted users. </para></listitem>
</varlistentry>

<varlistentry>
<term>-U|--user=username[%password]</term>
<listitem><para>Sets the SMB username or username and password. </para>

<para>If %password is not specified, the user will be prompted. The
client will first check the <envar>USER</envar> environment variable, then the
<envar>LOGNAME</envar> variable and if either exists, the
string is uppercased. If these environmental variables are not
found, the username <constant>GUEST</constant> is used. </para>

<para>A third option is to use a credentials file which
contains the plaintext of the username and password.  This
option is mainly provided for scripts where the admin does not
wish to pass the credentials on the command line or via environment
variables. If this method is used, make certain that the permissions
on the file restrict access from unwanted users.  See the
<parameter moreinfo="none">-A</parameter> for more details. </para>

<para>Be cautious about including passwords in scripts. Also, on
many systems the command line of a running process may be seen
via the <literal>ps</literal> command.  To be safe always allow
<literal>rpcclient</literal> to prompt for a password and type
it in directly. </para></listitem>
</varlistentry>


<varlistentry>
<term>-S|--signing on|off|required</term>
<listitem><para>Set the client signing state.
</para></listitem>
</varlistentry>


<varlistentry>
<term>-P|--machine-pass</term>
<listitem><para>Use stored machine account password.
</para></listitem>
</varlistentry>


<varlistentry>
<term>-e|--encrypt</term>
<listitem><para>
This command line parameter requires the remote
server support the UNIX extensions or that the SMB3 protocol has been selected.
Requests that the connection be encrypted. Negotiates SMB encryption using either
SMB3 or POSIX extensions via GSSAPI. Uses the given credentials for
the encryption negotiation (either kerberos or NTLMv1/v2 if given
domain/username/password triple. Fails the connection if encryption
cannot be negotiated.
</para></listitem>
</varlistentry>


<varlistentry>
<term>--pw-nt-hash</term>
<listitem><para>The supplied password is the NT hash.
</para></listitem>
</varlistentry>


		

<varlistentry>
<term>-?|--help</term>
<listitem><para>Print a summary of command line options.
</para></listitem>
</varlistentry>


<varlistentry>
<term>--usage</term>
<listitem><para>Display brief usage message.
</para></listitem>
</varlistentry>


		
	</variablelist>
</refsect1>

<refsect1>
	<title>VERSION</title>

	<para>This man page is part of version 4.10.11 of the Samba
	suite.</para>
</refsect1>

<refsect1>
	<title>AUTHOR</title>
	
	<para>The original Samba software and related utilities 
	were created by Andrew Tridgell. Samba is now developed
	by the Samba Team as an Open Source project similar 
	to the way the Linux kernel is developed.</para>
	
	<para>The smbtree man page was written by Jelmer Vernooij. </para>
</refsect1>

</refentry>
