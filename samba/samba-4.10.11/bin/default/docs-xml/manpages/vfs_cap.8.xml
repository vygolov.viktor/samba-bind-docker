<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<refentry id="vfs_cap.8">

<refmeta>
	<refentrytitle>vfs_cap</refentrytitle>
	<manvolnum>8</manvolnum>
	<refmiscinfo class="source">Samba</refmiscinfo>
	<refmiscinfo class="manual">System Administration tools</refmiscinfo>
	<refmiscinfo class="version">4.10.11</refmiscinfo>
</refmeta>


<refnamediv>
	<refname>vfs_cap</refname>
	<refpurpose>CAP encode filenames</refpurpose>
</refnamediv>

<refsynopsisdiv>
	<cmdsynopsis sepchar=" ">
		<literal>vfs objects = cap</literal>
	</cmdsynopsis>
</refsynopsisdiv>

<refsect1>
	<title>DESCRIPTION</title>

	<para>This VFS module is part of the
	<citerefentry><refentrytitle>samba</refentrytitle>
	<manvolnum>7</manvolnum></citerefentry> suite.</para>

	<para>CAP (Columbia Appletalk Protocol) encoding is a
	technique for representing non-ASCII filenames in ASCII. The
	<literal>vfs_cap</literal> VFS module translates filenames to and
	from CAP format, allowing users to name files in their native
	encoding.  </para>

	<para>CAP encoding is most commonly
	used in Japanese language environments. </para>

	<para>This module is stackable.</para>

</refsect1>


<refsect1>
	<title>EXAMPLES</title>

	<para>On a system using GNU libiconv, use CAP encoding to support
	users in the Shift_JIS locale:</para>

<programlisting format="linespecific">
        <parameter>[global]</parameter>
	<link xmlns:xlink="http://www.w3.org/1999/xlink" linkend="DOSCHARSET" xlink:href="smb.conf.5.html#DOSCHARSET">dos charset = CP932</link>
	<link xmlns:xlink="http://www.w3.org/1999/xlink" linkend="DOSCHARSET" xlink:href="smb.conf.5.html#DOSCHARSET">dos charset = CP932</link>
	<link xmlns:xlink="http://www.w3.org/1999/xlink" linkend="VFSOBJECTS" xlink:href="smb.conf.5.html#VFSOBJECTS">vfs objects = cap</link>
</programlisting>

</refsect1>

<refsect1>
	<title>VERSION</title>

	<para>This man page is part of version 4.10.11 of the Samba suite.
	</para>
</refsect1>

<refsect1>
	<title>AUTHOR</title>

	<para>The original Samba software and related utilities
	were created by Andrew Tridgell. Samba is now developed
	by the Samba Team as an Open Source project similar
	to the way the Linux kernel is developed.</para>

</refsect1>

</refentry>
