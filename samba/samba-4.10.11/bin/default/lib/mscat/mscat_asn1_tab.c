#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <libtasn1.h>

const asn1_static_node mscat_asn1_tab[] = {
  { "CATALOG", 536875024, NULL },
  { NULL, 1073741836, NULL },
  { "CatalogNameValue", 1610612741, NULL },
  { "name", 1073741857, NULL },
  { "flags", 1073741827, NULL },
  { "value", 7, NULL },
  { "CatalogMemberInfo", 1610612741, NULL },
  { "name", 1073741857, NULL },
  { "id", 3, NULL },
  { "CatalogMemberInfo2", 1610612741, NULL },
  { "memId", 1073741836, NULL },
  { "unknown", 536870927, NULL },
  { NULL, 2, "SpcLink"},
  { "SpcIndirectData", 1610612741, NULL },
  { "data", 1073741826, "SpcAttributeTypeAndOptionalValue"},
  { "messageDigest", 2, "DigestInfo"},
  { "SpcAttributeTypeAndOptionalValue", 1610612741, NULL },
  { "type", 1073741836, NULL },
  { "value", 541081613, NULL },
  { "type", 1, NULL },
  { "DigestInfo", 1610612741, NULL },
  { "digestAlgorithm", 1073741826, "AlgorithmIdentifier"},
  { "digest", 7, NULL },
  { "AlgorithmIdentifier", 1610612741, NULL },
  { "algorithm", 1073741836, NULL },
  { "parameters", 541081613, NULL },
  { "algorithm", 1, NULL },
  { "SpcPEImageData", 1610612741, NULL },
  { "flags", 1610645506, "SpcPeImageFlags"},
  { NULL, 9, "includeResources"},
  { "link", 536895490, "SpcLink"},
  { NULL, 2056, "0"},
  { "SpcPeImageFlags", 1610874886, NULL },
  { "includeResources", 1073741825, "0"},
  { "includeDebugInfo", 1073741825, "1"},
  { "includeImportAddressTable", 1, "2"},
  { "SpcLink", 1610612754, NULL },
  { "url", 1610620957, NULL },
  { NULL, 4104, "0"},
  { "moniker", 1610620930, "SpcSerializedObject"},
  { NULL, 4104, "1"},
  { "file", 536879106, "SpcString"},
  { NULL, 2056, "2"},
  { "SpcSerializedObject", 1610612741, NULL },
  { "classId", 1073741831, NULL },
  { "data", 7, NULL },
  { "SpcString", 1610612754, NULL },
  { "unicode", 1610620961, NULL },
  { NULL, 4104, "0"},
  { "ascii", 536879133, NULL },
  { NULL, 4104, "1"},
  { "SpcImageDataFile", 1610612741, NULL },
  { "flags", 1073741830, NULL },
  { "file", 2, "SpcLink"},
  { "CatalogListId", 1610612741, NULL },
  { "oid", 12, NULL },
  { "CatalogListMemberId", 1610612741, NULL },
  { "oid", 1073741836, NULL },
  { "optional", 20, NULL },
  { "MemberAttribute", 1610612741, NULL },
  { "contentType", 1073741836, NULL },
  { "content", 536870927, NULL },
  { NULL, 541065229, NULL },
  { "contentType", 1, NULL },
  { "CatalogListMember", 1610612741, NULL },
  { "checksum", 1073741831, NULL },
  { "attributes", 536887311, NULL },
  { NULL, 2, "MemberAttribute"},
  { "CatalogAttribute", 1610612741, NULL },
  { "dataId", 1073741836, NULL },
  { "encapsulated_data", 7, NULL },
  { "CertTrustList", 536870917, NULL },
  { "catalogListId", 1073741826, "CatalogListId"},
  { "unknownString", 1073741831, NULL },
  { "trustUtcTime", 1073741860, NULL },
  { "catalogListMemberId", 1073741826, "CatalogListMemberId"},
  { "members", 1610612747, NULL },
  { NULL, 2, "CatalogListMember"},
  { "attributes", 536895499, NULL },
  { NULL, 1073743880, "0"},
  { NULL, 2, "CatalogAttribute"},
  { NULL, 0, NULL }
};
