<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<refentry id="vfs_shadow_copy.8">

<refmeta>
	<refentrytitle>vfs_shadow_copy</refentrytitle>
	<manvolnum>8</manvolnum>
	<refmiscinfo class="source">Samba</refmiscinfo>
	<refmiscinfo class="manual">System Administration tools</refmiscinfo>
	<refmiscinfo class="version">4.10.11</refmiscinfo>
</refmeta>


<refnamediv>
	<refname>vfs_shadow_copy</refname>
	<refpurpose>Expose snapshots to Windows clients as shadow copies.</refpurpose>
</refnamediv>

<refsynopsisdiv>
	<cmdsynopsis sepchar=" ">
		<literal>vfs objects = shadow_copy</literal>
	</cmdsynopsis>
</refsynopsisdiv>

<refsect1>
	<title>DESCRIPTION</title>

	<para>This VFS module is part of the
	<citerefentry><refentrytitle>samba</refentrytitle>
	<manvolnum>7</manvolnum></citerefentry> suite.</para>

	<para>The <literal>vfs_shadow_copy</literal> VFS module functionality
	that is similar to Microsoft Shadow Copy services. When setup properly,
	this module allows Microsoft Shadow Copy clients to browse
	"shadow copies" on Samba shares.
	</para>

	<para>This module is stackable.</para>

</refsect1>

<refsect1>
	<title>CONFIGURATION</title>

	<para><literal>vfs_shadow_copy</literal> relies on a filesystem
	snapshot implementation. Many common filesystems have native
	support for this.
	</para>

	<para>Filesystem snapshots must be mounted on
	specially named directories in order to be recognized by
	<literal>vfs_shadow_copy</literal>. The snapshot mount points must
	be immediate children of a the directory being shared.</para>

	<para>The snapshot naming convention is @GMT-YYYY.MM.DD-hh.mm.ss,
	where:
       	<itemizedlist>
       		<listitem><para><literal>YYYY</literal> is the 4 digit year</para></listitem>
       		<listitem><para><literal>MM</literal> is the 2 digit month</para></listitem>
       		<listitem><para><literal>DD</literal> is the 2 digit day</para></listitem>
       		<listitem><para><literal>hh</literal> is the 2 digit hour</para></listitem>
       		<listitem><para><literal>mm</literal> is the 2 digit minute</para></listitem>
       		<listitem><para><literal>ss</literal> is the 2 digit second.</para></listitem>
       	</itemizedlist>
	</para>

       	<para>The <literal>vfs_shadow_copy</literal> snapshot naming convention can be produced with the following
	<citerefentry><refentrytitle>date</refentrytitle>
	<manvolnum>1</manvolnum></citerefentry> command:
	<programlisting format="linespecific">
	TZ=GMT date +@GMT-%Y.%m.%d-%H.%M.%S
	</programlisting></para>

</refsect1>

<refsect1>
	<title>EXAMPLES</title>

	<para>Add shadow copy support to user home directories:</para>
<programlisting format="linespecific">
        <parameter>[homes]</parameter>
	<link xmlns:xlink="http://www.w3.org/1999/xlink" linkend="VFSOBJECTS" xlink:href="smb.conf.5.html#VFSOBJECTS">vfs objects = shadow_copy</link>
</programlisting>

</refsect1>

<refsect1>
	<title>CAVEATS</title>

	<para>This is not a backup, archival, or version control solution.
	</para>

	<para>With Samba or Windows servers,
	<literal>vfs_shadow_copy</literal> is designed to be an end-user
	tool only. It does not replace or enhance your backup and
	archival solutions and should in no way be considered as
	such. Additionally, if you need version control, implement a
	version control system.</para>

</refsect1>



<refsect1>
	<title>VERSION</title>

	<para>This man page is part of version 4.10.11 of the Samba suite.
	</para>
</refsect1>

<refsect1>
	<title>AUTHOR</title>

	<para>The original Samba software and related utilities
	were created by Andrew Tridgell. Samba is now developed
	by the Samba Team as an Open Source project similar
	to the way the Linux kernel is developed.</para>

</refsect1>

</refentry>
