#!/bin/bash
curl -OL https://download.samba.org/pub/samba/samba-$SAMBA_VERSION.tar.gz && tar xf samba-$SAMBA_VERSION.tar.gz
cd samba-$SAMBA_VERSION && ./configure && make && make install
