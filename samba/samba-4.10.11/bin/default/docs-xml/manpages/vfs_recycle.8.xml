<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<refentry id="vfs_recycle.8">

<refmeta>
	<refentrytitle>vfs_recycle</refentrytitle>
	<manvolnum>8</manvolnum>
	<refmiscinfo class="source">Samba</refmiscinfo>
	<refmiscinfo class="manual">System Administration tools</refmiscinfo>
	<refmiscinfo class="version">4.10.11</refmiscinfo>
</refmeta>


<refnamediv>
	<refname>vfs_recycle</refname>
	<refpurpose>Samba VFS recycle bin</refpurpose>
</refnamediv>

<refsynopsisdiv>
	<cmdsynopsis sepchar=" ">
		<literal>vfs objects = recycle</literal>
	</cmdsynopsis>
</refsynopsisdiv>

<refsect1>
	<title>DESCRIPTION</title>

	<para>This VFS module is part of the
	<citerefentry><refentrytitle>samba</refentrytitle>
	<manvolnum>7</manvolnum></citerefentry> suite.</para>

	<para>The <literal>vfs_recycle</literal> intercepts file deletion
	requests and moves the affected files to a temporary repository
	rather than deleting them immediately. This gives the same effect
	as the Recycle Bin on Windows computers. </para>

	<para>The Recycle Bin will not appear in Windows Explorer
	views of the network file system (share) nor on any mapped
	drive. Instead, a directory called .recycle will be automatically
	created when the first file is deleted and recycle:repository is
	not configured. If recycle:repository is configured, the name
	of the created directory depends on recycle:repository. Users
	can recover files from the recycle bin. If the recycle:keeptree
	option has been specified, deleted files will be found in a path
	identical with that from which the file was deleted.  </para>


	<para>This module is stackable.</para>

</refsect1>


<refsect1>
	<title>OPTIONS</title>

	<variablelist>

		<varlistentry>
		<term>recycle:repository = PATH</term>
		<listitem>
		<para>Path of the directory where deleted files should be moved.
		</para>
		<para>If this option is not set, the default path .recycle
		is used. </para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:directory_mode = MODE</term>
		<listitem>
		<para>Set MODE to the octal mode the recycle repository
		should be created with.  The recycle repository will be
		created when first file is deleted. If recycle:subdir_mode
		is not set, MODE also applies to subdirectories.
		</para>
		<para>If this option is not set, the default mode
		0700 is used. </para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:subdir_mode = MODE</term>
		<listitem>
		<para>Set MODE to the octal mode with which
		sub directories of the recycle repository should be created.
		</para>
		<para>If this option is not set, subdirectories
		will be created with the mode from recycle:directory_mode.
		</para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:keeptree = BOOL</term>
		<listitem>
		<para>Specifies whether the directory structure should
		be preserved or whether the files in a directory that is being
		deleted should be kept separately in the repository.
		</para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:versions = BOOL</term>
		<listitem>
		<para>If this option is True, two files with the same
		name that are deleted will both be kept in the repository.
		Newer deleted versions of a file will be called
		"Copy #x of filename".
		</para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:touch = BOOL</term>
		<listitem>
		<para>Specifies whether a file's access date should be
		updated when the file is moved to the repository.
		</para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:touch_mtime = BOOL</term>
		<listitem>
		<para>Specifies whether a file's last modified date should be
		updated when the file is moved to the repository.
		</para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:minsize = BYTES</term>
		<listitem>
		<para>Files that are smaller than the number of bytes
		specified by this parameter will not be put into the
		repository.
		</para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:maxsize = BYTES</term>
		<listitem>
		<para>Files that are larger than the number of bytes
		specified by this parameter will not be put into the
		repository.
		</para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:exclude = LIST</term>
		<listitem>
		<para>List of files that should not be put into the
		repository when deleted, but deleted in the normal way.
		Wildcards such as * and ? are supported.
		</para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:exclude_dir = LIST</term>
		<listitem>
		<para>List of directories whose files should not be put
		into the repository when deleted, but deleted in the
		normal way. Wildcards such as * and ? are supported.
		</para>
		</listitem>
		</varlistentry>

		<varlistentry>
		<term>recycle:noversions = LIST</term>
		<listitem>
		<para>Specifies a list of paths (wildcards such as *
		and ? are supported) for which no versioning should
		be used. Only useful when recycle:versions is enabled.
		</para>
		</listitem>
		</varlistentry>

	</variablelist>
</refsect1>

<refsect1>
	<title>EXAMPLES</title>

	<para>Move files "deleted" on <parameter moreinfo="none">share</parameter> to
	<parameter moreinfo="none">/data/share/.recycle</parameter> instead of deleting them:
	</para>

<programlisting format="linespecific">
        <parameter>[share]</parameter>
	<link xmlns:xlink="http://www.w3.org/1999/xlink" linkend="PATH" xlink:href="smb.conf.5.html#PATH">path = /data/share</link>
	<link xmlns:xlink="http://www.w3.org/1999/xlink" linkend="VFSOBJECTS" xlink:href="smb.conf.5.html#VFSOBJECTS">vfs objects = recycle</link>
	<link xmlns:xlink="http://www.w3.org/1999/xlink" linkend="RECYCLE:REPOSITORY" xlink:href="smb.conf.5.html#RECYCLE:REPOSITORY">recycle:repository = .recycle</link>
	<link xmlns:xlink="http://www.w3.org/1999/xlink" linkend="RECYCLE:KEEPTREE" xlink:href="smb.conf.5.html#RECYCLE:KEEPTREE">recycle:keeptree = yes</link>
	<link xmlns:xlink="http://www.w3.org/1999/xlink" linkend="RECYCLE:VERSIONS" xlink:href="smb.conf.5.html#RECYCLE:VERSIONS">recycle:versions = yes</link>
</programlisting>

</refsect1>

<refsect1>
	<title>VERSION</title>

	<para>This man page is part of version 4.10.11 of the Samba suite.
	</para>
</refsect1>

<refsect1>
	<title>AUTHOR</title>

	<para>The original Samba software and related utilities
	were created by Andrew Tridgell. Samba is now developed
	by the Samba Team as an Open Source project similar
	to the way the Linux kernel is developed.</para>

</refsect1>

</refentry>
