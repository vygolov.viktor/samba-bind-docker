#!/bin/bash

/usr/sbin/sshd

if [[ -f "/usr/local/samba/etc/smb.conf" ]]; then
	while true; do
		TEST=`ps aux | awk '{print $11}' | grep osync`
		if [[ -z "$TEST" ]]; then
			osync.sh /etc/osync/sync.conf
		fi
		sleep 300
	done
else
	bash
fi
